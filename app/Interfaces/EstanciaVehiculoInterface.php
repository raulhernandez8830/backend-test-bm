<?php

namespace App\Interfaces;

interface EstanciaVehiculoInterface{

    public function save($data);
    public function update($data, $id);

}
