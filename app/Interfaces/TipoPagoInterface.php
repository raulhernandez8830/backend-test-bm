<?php

namespace App\Interfaces;

interface TipoPagoInterface{
    public function getAllTipoPagos();
    public function getTipoPago($tipoPagoId);
    public function save($data);
    public function update($data, $id);
    public function delete($id);
    public function findById($id);
}
