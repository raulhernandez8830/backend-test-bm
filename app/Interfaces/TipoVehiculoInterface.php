<?php
namespace App\Interfaces;
interface TipoVehiculoInterface{
    public function getAll();
    public function save($data);
    public function update($data, $id);
    public function delete($id);
    public function findById($id);
}
