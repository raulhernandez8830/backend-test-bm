<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoVehiculo extends Model
{
    use HasFactory;

    public function tipo_pagos()
    {
        return $this->belongsTo(TipoPago::class);
    }

    protected $fillable = [
        'tipo',
        'tipo_pago_id'
    ];
}
