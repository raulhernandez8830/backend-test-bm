<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoPago extends Model
{
    use HasFactory;

    public function tipo_vehiculos()
    {
        return $this->hasMany(TipoVehiculo::class);
    }

    protected $fillable = [
        'nombre',
        'tarifa_minuto'

    ];
}
