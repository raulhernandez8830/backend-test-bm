<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstanciaVehiculo extends Model
{
    use HasFactory;

    protected $fillable = [
        'placa',
        'hora_entrada',
        'hora_salida',
        'tipo_vehiculo_id'
    ];
}
