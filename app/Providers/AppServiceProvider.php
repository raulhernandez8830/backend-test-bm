<?php

namespace App\Providers;

use App\Interfaces\EstanciaVehiculoInterface;
use App\Interfaces\TipoPagoInterface;
use App\Interfaces\TipoVehiculoInterface;
use App\Interfaces\VehiculoInterface;
use App\Repositories\EstanciaVehiculoRepository;
use App\Repositories\TipoPagoRepository;
use App\Repositories\VehiculoRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\TipoVehiculoRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(TipoPagoInterface::class, TipoPagoRepository::class);
        $this->app->bind(TipoVehiculoInterface::class, TipoVehiculoRepository::class);
        $this->app->bind(VehiculoInterface::class, VehiculoRepository::class);
        $this->app->bind(EstanciaVehiculoInterface::class, EstanciaVehiculoRepository::class);

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
