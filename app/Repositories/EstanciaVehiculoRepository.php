<?php

namespace App\Repositories;

use App\Interfaces\EstanciaVehiculoInterface;
use App\Models\EstanciaVehiculo;

class EstanciaVehiculoRepository implements EstanciaVehiculoInterface
{

    public function save($data)
    {
        return EstanciaVehiculo::create($data);
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }
}
