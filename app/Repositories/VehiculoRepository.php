<?php

namespace App\Repositories;
use App\Interfaces\VehiculoInterface;
use App\Models\Vehiculo;

class VehiculoRepository implements VehiculoInterface
{

    public function getAll()
    {
        return Vehiculo::all();
    }

    public function save($data)
    {
        return Vehiculo::create($data);
    }

    public function update($data, $id)
    {
        return Vehiculo::whereId($id)->update($data);
    }

    public function delete($id)
    {
        return Vehiculo::destroy($id);
    }

    public function findById($id)
    {
        return Vehiculo::findOrFail($id);
    }

    public function getVehiculoXPlaca($placa)
    {
        $vehiculo = Vehiculo::where('placa',$placa)->first();
        return $vehiculo;
    }
}
