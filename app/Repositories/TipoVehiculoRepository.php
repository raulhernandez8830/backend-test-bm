<?php

namespace App\Repositories;


use App\Interfaces\TipoVehiculoInterface;
use App\Http\Controllers\TipoPagoController;
use App\Models\TipoVehiculo;

class TipoVehiculoRepository implements TipoVehiculoInterface
{


    public function getAll()
    {
        return TipoVehiculo::all();
    }

    public function save($data)
    {
        return TipoVehiculo::create($data);
    }

    public function update($data, $id)
    {
        return TipoVehiculo::whereId($id)->update($data);
    }

    public function delete($id)
    {
        return TipoVehiculo::destroy($id);
    }

    public function findById($id)
    {
        return  TipoVehiculo::findOrFail($id);

    }
}
