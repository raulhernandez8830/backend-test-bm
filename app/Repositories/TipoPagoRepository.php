<?php

namespace App\Repositories;

use App\Interfaces\TipoPagoInterface;
use App\Models\TipoPago;

class TipoPagoRepository implements TipoPagoInterface
{

    public function getAllTipoPagos()
    {
        return TipoPago::all();
    }

    public function getTipoPago($tipoPagoId)
    {
        // TODO: Implement getTipoPago() method.
    }

    public function save($data)
    {
        return TipoPago::create($data);
    }

    public function update($data, $id)
    {
        return TipoPago::whereId($id)->update($data);
    }

    public function delete($id)
    {
        return TipoPago::destroy($id);
    }

    public function findById($id)
    {
        $data =  TipoPago::findOrFail($id);
        return $data->tipo_vehiculos;
    }
}
