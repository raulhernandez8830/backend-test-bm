<?php

namespace App\Http\Controllers;

use App\Interfaces\VehiculoInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class VehiculoController extends Controller
{

    private VehiculoInterface $vehiculoRepository;

    public function __construct(VehiculoInterface $vehiculoRepository)
    {
        $this->vehiculoRepository = $vehiculoRepository;
    }


    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json([
           'data'=> $this->vehiculoRepository->getAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $datos = $request->only([
            'placa',
            'tipo_vehiculo_id'
        ]);

        return response()->json([
            $this->vehiculoRepository->save($datos)
        ],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request): JsonResponse
    {
        $id = $request->route('id');
        return response()->json(['data' => $this->vehiculoRepository->findById($id)]);
    }

    public function getVehiculoXPlaca(Request $request): JsonResponse
    {
        $placa  = $request->route('placa');
        return response()->json([
           'data'=>$this->vehiculoRepository->getVehiculoXPlaca($placa)
        ],Response::HTTP_ACCEPTED);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
        $id = $request->route('id');
        $vehiculo = $request->only([
            'placa',
            'tipo_vehiculo_id'
        ]);

        return response()->json([
           'data'=> $this->vehiculoRepository->update($vehiculo, $id)
        ], Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $id = $request->route('id');
        $this->vehiculoRepository->delete($id);
        return response()->json([null, Response::HTTP_NO_CONTENT]);
    }
}
