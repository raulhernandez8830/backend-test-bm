<?php

namespace App\Http\Controllers;

use App\Interfaces\TipoPagoInterface;
use App\Repositories\TipoPagoRepository;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TipoPagoController extends Controller
{

    private TipoPagoInterface $tipoPagoRepository;

    public function __construct(TipoPagoInterface $tipoPagoRepository)
    {
        $this->tipoPagoRepository = $tipoPagoRepository;

    }


    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json([
           'datos' => $this->tipoPagoRepository->getAllTipoPagos()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $datos = $request->only([
            'nombre',
            'tarifa_minuto'
        ]);

        return response()->json([
          'data' => $this->tipoPagoRepository->save($datos)
        ],
            Response::HTTP_CREATED
        );

    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request): JsonResponse
    {
        $id = $request->route('id');
        return response()->json([
            'data' => $this->tipoPagoRepository->findById($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $id = $request->route('id');
        $tipopago = $request->only([
            'nombre',
            'tarifa_minuto'
        ]);

        return response()->json([
            'data' => $this->tipoPagoRepository->update($tipopago, $id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request): JsonResponse
    {
        $id = $request->route('id');
        $this->tipoPagoRepository->delete($id);
        return response()->json(null, Response::HTTP_NO_CONTENT);

    }
}
