<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Interfaces\EstanciaVehiculoInterface;
use Illuminate\Http\Response;

class EstanciaVehiculoController extends Controller
{
    private EstanciaVehiculoInterface $estanciaVehiculoRepository;

    public function __construct(EstanciaVehiculoInterface $estanciaVehiculoRepository)
    {
        $this->estanciaVehiculoRepository = $estanciaVehiculoRepository;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request):JsonResponse
    {
        $data = $request->only([
            'placa',
            'hora_entrada',
            'hora_salida',
            'tipo_vehiculo_id'

        ]);

        return response()->json([
           'data'=>$this->estanciaVehiculoRepository->save($data)
        ],Response::HTTP_CREATED);


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
