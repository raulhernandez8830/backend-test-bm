<?php

namespace App\Http\Controllers;

use App\Interfaces\TipoVehiculoInterface;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psy\Util\Json;

class TipoVehiculoController extends Controller
{
    private TipoVehiculoInterface $tipoVehiculoRepository;


    public function __construct(TipoVehiculoInterface $tipoVehiculoRepository){
        $this->tipoVehiculoRepository = $tipoVehiculoRepository;
    }


    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'data'=> $this->tipoVehiculoRepository->getAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $datos = $request->only([
            'tipo',
            'tipo_pago_id'
        ]);

        return response()->json([
            $this->tipoVehiculoRepository->save($datos)
        ],
        Response::HTTP_CREATED
    );
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $id = $request->route('id');
        $tipovehiculo = $request->only([
            'tipo',
            'tipo_pago_id'
        ]);

        return response()->json([
            'data' => $this->tipoVehiculoRepository->update($tipovehiculo, $id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $id = $request->route('id');
        $this->tipoVehiculoRepository->delete($id);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
