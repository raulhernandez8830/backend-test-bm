<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TipoPagoController;
use App\Http\Controllers\TipoVehiculoController;
use App\Http\Controllers\VehiculoController;
use App\Http\Controllers\EstanciaVehiculoController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*-----------------------------------------------------------
        Rutas para la funcionalidad de tipos de pago
------------------------------------------------------------*/

Route::get('/tipo_pagos', [TipoPagoController::class,'index']);
Route::post('/tipo_pagos', [TipoPagoController::class, 'store']);
Route::put('/tipo_pagos/{id}',[TipoPagoController::class,'update']);
Route::delete('/tipo_pagos/{id}',[TipoPagoController::class,'destroy']);
Route::get('/tipo_pagos/{id}',[TipoPagoController::class,'show']);

/*-----------------------------------------------------------*/



/*-----------------------------------------------------------
        Rutas para la funcionalidad de tipos de vehiculo
------------------------------------------------------------*/

Route::get('/tipo_vehiculos', [TipoVehiculoController::class,'index']);
Route::post('/tipo_vehiculos',[TipoVehiculoController::class,'store']);
Route::put('/tipo_vehiculos/{id}',[TipoVehiculoController::class,'update']);
Route::delete('/tipo_vehiculos/{id}',[TipoVehiculoController::class,'destroy']);
Route::get('/vehiculos/{id}',[VehiculoController::class,'show']);

/*-----------------------------------------------------------*/



/*-----------------------------------------------------------
        Rutas para la funcionalidad de vehiculos
------------------------------------------------------------*/

Route::get('/vehiculos', [VehiculoController::class,'index']);
Route::post('/vehiculos',[VehiculoController::class,'store']);
Route::put('/vehiculos/{id}',[VehiculoController::class,'update']);
Route::delete('/vehiculos/{id}',[VehiculoController::class,'destroy']);
Route::get('/vehiculos/vehiculoxplaca/{placa}',[VehiculoController::class,'getVehiculoXPlaca']);

/*-----------------------------------------------------------*/



/*-----------------------------------------------------------
        Rutas para la funcionalidad de vehiculos
------------------------------------------------------------*/

Route::post('/estancia-vehiculos', [EstanciaVehiculoController::class,'store']);


/*-----------------------------------------------------------*/
