<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('estancia_vehiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('placa');
            $table->time('hora_entrada');
            $table->time('hora_salida');
            $table->unsignedInteger('tipo_vehiculo_id');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('tipo_vehiculo_id')->references('id')->on('tipo_vehiculos');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('estancia_vehiculos');
    }
};
